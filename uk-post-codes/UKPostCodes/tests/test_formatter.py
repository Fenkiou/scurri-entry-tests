import unittest

from UKPostCodes.formatter import Formatter


class TestFormatter(unittest.TestCase):
    def setUp(self):
        self.formatter = Formatter()

    def test_extract_infos_method_exists(self):
        method_exists = hasattr(self.formatter, 'extract_infos')
        self.assertEqual(method_exists, True)

    def test_postcode_is_valid(self):
        informations = self.formatter.extract_infos('OX1 1AA')
        self.assertIsInstance(informations, dict)

    def test_postcode_is_not_valid(self):
        is_valid = self.formatter.extract_infos('QX1 1AA')
        self.assertEqual(is_valid, "Postcode is not valid")

        with self.assertRaisesRegex(TypeError, "Postcode must be a string"):
            self.formatter.extract_infos(123)

    def test_extract_infos_return_outward_code(self):
        informations = self.formatter.extract_infos('OX1 1AA')
        self.assertEqual(informations['outward_code'], 'OX1')

    def test_extract_infos_return_inward_code(self):
        informations = self.formatter.extract_infos('OX1 1AA')
        self.assertEqual(informations['inward_code'], '1AA')

    def test_extract_infos_return_postcode_area(self):
        informations = self.formatter.extract_infos('OX1 1AA')
        self.assertEqual(informations['postcode_area'], 'OX')

    def test_extract_infos_return_postcode_district(self):
        informations = self.formatter.extract_infos('OX1 1AA')
        self.assertEqual(informations['postcode_district'], 'OX1')

    def test_extract_infos_return_postcode_sector(self):
        informations = self.formatter.extract_infos('OX1 1AA')
        self.assertEqual(informations['postcode_sector'], 'OX1 1')

    def test_extract_infos_return_postcode_unit(self):
        informations = self.formatter.extract_infos('OX1 1AA')
        self.assertEqual(informations['postcode_unit'], 'AA')

if __name__ == '__main__':
    unittest.main()

import re

class Formatter():

    def __init__(self):
        self.re_uk_postcode = re.compile('(GIR ?0AA|SAN ?TA1|(?:[A-PR-UWYZ](?:\d{0,2}|[A-HK-Y]\d|[A-HK-Y]\d\d|\d[A-HJKSTUW]|[A-HK-Y]\d[ABEHMNPRV-Y])) ?\d[ABD-HJLNP-UW-Z]{2})')
        self.re_postcode_area = re.compile('[a-zA-Z]{2}')

    def _extract_outward_inward_codes(self, postcode):
        postcode = postcode.replace(' ', '')
        string_length = len(postcode)

        outward_code = postcode[0:string_length - 3]
        inward_code = postcode[string_length - 3:string_length]

        return outward_code, inward_code

    def _extract_postcode_area(self, outward_code):
        if not self.re_postcode_area.match(outward_code[0:2]):
            return outward_code[0:1]

        return outward_code[0:2]

    def _extract_postcode_sector(self, postcode_district, inward_code):
        return postcode_district + ' ' + inward_code[0:1]

    def _extract_postcode_unit(self, inward_code):
        return inward_code[1:3]

    def _validate(self, postcode):
        try:
            if not self.re_uk_postcode.match(postcode) == None:
                return True
        except TypeError:
            raise ValueError

        return False

    def extract_infos(self, postcode):
        if not self._validate(postcode):
            return "Postcode is not valid"

        outward_code, inward_code = self._extract_outward_inward_codes(postcode)
        postcode_area = self._extract_postcode_area(outward_code)
        postcode_district = outward_code
        postcode_sector = self._extract_postcode_sector(postcode_district, inward_code)
        postcode_unit = self._extract_postcode_unit(inward_code)

        informations = {
            'outward_code': outward_code,
            'inward_code': inward_code,
            'postcode_area': postcode_area,
            'postcode_district': postcode_district,
            'postcode_sector': postcode_sector,
            'postcode_unit': postcode_unit
        }
        return informations

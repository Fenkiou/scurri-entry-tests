from distutils.core import setup

setup(name = "UKPostCodes",
    version = "0.0.1",
    description = "A library that can validate and extract informations from a UK postcode",
    author = "Alexandre Papin",
    author_email = "alexandre.papin@indaclouds.fr",
    packages = ['UKPostCodes'],
)
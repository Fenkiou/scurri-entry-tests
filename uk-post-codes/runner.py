import os
import json

from UKPostCodes.formatter import Formatter

root_path = os.path.dirname(__file__)
sample_path = 'UKPostCodes/data/sample.json'
sample = None

with open(os.path.join(root_path, sample_path)) as data_file:
    sample = json.load(data_file)

formatter = Formatter()

for postcode in sample:
    print(formatter.extract_infos(postcode['postcode']))

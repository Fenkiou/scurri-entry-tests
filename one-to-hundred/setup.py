from distutils.core import setup

setup(name = "OneToHundred",
    version = "0.0.1",
    description = "A program that prints the numbers from 1 to 100.",
    author = "Alexandre Papin",
    author_email = "alexandre.papin@indaclouds.fr",
    packages = ['OneToHundred'],
)
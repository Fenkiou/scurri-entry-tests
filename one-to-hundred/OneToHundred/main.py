def OneToHundred():
    value = None

    for number in range(1, 101):
        if multiple_of_both(number):
            value = "ThreeFive"
        elif multiple_of_3(number):
            value = "Three"
        elif multiple_of_5(number):
            value = "Five"
        else:
            value = number

        print(value)

    return value


def multiple_of_3(number):
    return number % 3 == 0


def multiple_of_5(number):
    return number % 5 == 0


def multiple_of_both(number):
    if not multiple_of_3(number):
        return False
    if not multiple_of_5(number):
        return False
    return True

if __name__ == '__main__':
    OneToHundred()
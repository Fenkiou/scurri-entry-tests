import unittest

from OneToHundred.main import OneToHundred, multiple_of_3, multiple_of_5, multiple_of_both


class TestOneToHundred(unittest.TestCase):

    def test_last_value_is_100(self):
        last = OneToHundred()
        self.assertEqual(last, "Five")

    def test_9_is_multiple_of_3(self):
        result = multiple_of_3(9)
        self.assertEqual(result, True)

    def test_10_is_not_multiple_of_3(self):
        result = multiple_of_3(10)
        self.assertEqual(result, False)

    def test_20_is_multiple_of_5(self):
        result = multiple_of_5(20)
        self.assertEqual(result, True)

    def test_21_is_not_multiple_of_5(self):
        result = multiple_of_5(21)
        self.assertEqual(result, False)

    def test_15_is_multiple_of_both(self):
        result = multiple_of_both(15)
        self.assertEqual(result, True)

    def test_16_is_not_multiple_of_both(self):
        result = multiple_of_both(16)
        self.assertEqual(result, False)

if __name__ == '__main__':
    unittest.main()